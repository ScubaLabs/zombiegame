using UnityEngine;
using System.Collections;

public class Spell : MonoBehaviour {
	
	public Texture2D icon;
	public string spellName;
	public float cooldown;
	[HideInInspector]
	public float cooldownTimer;
	[HideInInspector]
	public bool onCooldown;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(cooldownTimer < cooldown)
			cooldownTimer += Time.deltaTime;
		if(cooldownTimer >= cooldown)
			onCooldown = false;
	}
	
	public void Use() {
		//Here you can implement your cool spells like a big tornado or a lightning hitting enemys.. It's all up to you. :)
		cooldownTimer = 0;
		onCooldown = true;
	}
}
