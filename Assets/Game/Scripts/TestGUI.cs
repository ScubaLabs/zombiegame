using UnityEngine;
using System.Collections;

public class TestGUI : MonoBehaviour {
	
	Player player;
	
	// Use this for initialization
	void Start () {
		//player = GameObject.Find("Player").GetComponent<Player>();
        player = GameObject.Find("OVRPlayerController").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update() {

        //if (GameManager.Instance.selectedEnemy != null)
        if (OVRInput.Get(OVRInput.Button.Three) || OVRInput.Get(OVRInput.Button.One) || OVRInput.Get(OVRInput.Button.Two) || OVRInput.Get(OVRInput.Button.Four) || Input.GetKeyUp(KeyCode.Return))
        {
            player.FireWeapon();
        }
    }
	
	void OnGUI()
	{
		if(GUI.Button(new Rect(20,20,150,30), "Shoot!"))
		{
			if(GameManager.Instance.selectedEnemy != null)
			{
				player.FireWeapon();
			}
		}
	}
	
}
