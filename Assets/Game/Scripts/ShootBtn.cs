﻿using UnityEngine;
using System.Collections;

public class ShootBtn {
	ShowMessageBox messageBox;

	// Use this for initialization
	void Start () {
		//messageBox = GetComponent<ShowMessageBox>();
		//messageBox.message = "Shooting";
		//messageBox.ShowMessage();
		//Application.LoadLevel("LevelSelect");
	}
	
	// Update is called once per frame
	void OnPress(bool isPressed) {
		if (isPressed) {
			HUD.instance.shooting = true;
		} else {
			HUD.instance.shooting = false;
		}
		//Application.LoadLevel("LevelSelect");
		//messageBox.message = "Shooting";
		//messageBox.ShowMessage();
	}
	
	void OnRelease() {
		HUD.instance.shooting = false;
	}
	

}
