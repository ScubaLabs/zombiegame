using UnityEngine;
using System.Collections;
using System.IO;

public class PlayerHelpSpawner : MonoBehaviour {
	
	public float spawnInterval = 15.0f;
	private float lastSpawnTime;
	
	public GameObject ammoPrefab;
	public GameObject healthPrefab;

	
	public ArrayList helpers = new ArrayList();
	// Use this for initialization
	void Start () 
	{
		lastSpawnTime = Time.timeSinceLevelLoad;
		//enemies.Add(spiderPrefab);
		helpers.Add(ammoPrefab);
		helpers.Add(healthPrefab);

		
		//enemies.Add(brainBug);
//		Debug.Log("Time at start of level"+ lastSpawnTime);
		SpawnHelp();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ((lastSpawnTime + spawnInterval) < Time.timeSinceLevelLoad )
		{
			SpawnHelp();
			lastSpawnTime = Time.timeSinceLevelLoad;
//			Debug.Log("LastSpawnTime:" + lastSpawnTime);
		}
	}
	
	void SpawnHelp()
	{
		int RandIndex = UnityEngine.Random.Range(0,helpers.Count);
		Debug.Log("RANDOMLY CHOSE: "+RandIndex);
		
		GameObject pickbug = helpers[RandIndex] as GameObject;
		GameObject bug = GameObject.Instantiate(pickbug, this.gameObject.transform.position, Quaternion.identity) as GameObject;
		
		//InputManager.Instance.touchableObjects.Add(bug);
		//InputManager.Instance.touchableObjects.Add(bug);
		
	}
}
