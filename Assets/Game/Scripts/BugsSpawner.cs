using UnityEngine;
using System.Collections;
using System.IO;

public class BugsSpawner : MonoBehaviour {
	
	public float spawnInterval = 15.0f;
	private float lastSpawnTime;
	
	public GameObject zombie1Prefab;
	public GameObject zombie2Prefab;
	public GameObject zombie3Prefab;
	public GameObject zombie4Prefab;
	
	public ArrayList enemies = new ArrayList();
	// Use this for initialization
	void Start () 
	{
		lastSpawnTime = Time.timeSinceLevelLoad;
		//enemies.Add(spiderPrefab);
		enemies.Add(zombie1Prefab);
		enemies.Add(zombie2Prefab);
		enemies.Add(zombie3Prefab);
		enemies.Add(zombie4Prefab);
		
		//enemies.Add(brainBug);
//		Debug.Log("Time at start of level"+ lastSpawnTime);
		SpawnBug();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ((lastSpawnTime + spawnInterval) < Time.timeSinceLevelLoad )
		{
			SpawnBug();
			lastSpawnTime = Time.timeSinceLevelLoad;
//			Debug.Log("LastSpawnTime:" + lastSpawnTime);
		}
	}
	
	void SpawnBug()
	{
		//return;
		int RandIndex = UnityEngine.Random.Range(0,enemies.Count);
		Debug.Log("RANDOMLY CHOSE: "+RandIndex);
		
		GameObject pickbug = enemies[RandIndex] as GameObject;
		GameObject bug = GameObject.Instantiate(pickbug, this.gameObject.transform.position, Quaternion.identity) as GameObject;
		bug.layer = LayerMask.NameToLayer("Default");
		bug.SendMessage("SetSpeed",8);
		GameObject title = GameObject.Find("TitleManager");
		if(title)
		{
		if(TitleManager.Instance.intro == true)
			{
				
				GameObject obj = GameObject.Find("GameManager");
				if(obj)
				{

				} else {
					bug.SendMessage("SetIntro",1);
				}
			}
		}
		//InputManager.Instance.touchableObjects.Add(bug);
		//InputManager.Instance.touchableObjects.Add(bug);
		
	}
}
