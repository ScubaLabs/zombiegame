﻿// Script made by Jens A. Kaalen
// www.hatefueled.net

// Leave walkAnim and/or attackAnim empty in the editor if you want random animations


var HP = 100;
var headHP = 20; // If headHP is reduced to 0 or less the zombie will die.

var runSpeed : float  = 5;
var attackDmg : int = 5;
var introwalk = false;

var attackSound : AudioClip;
var randomSound : AudioClip;
var randomSound2: AudioClip;
var ragDoll : Transform;
var ragDoll_headless : Transform;
var target : Transform;

var walkAnim : String;
var attackAnim : String;
var idleAnim : String;
var characterType : String;

private var distanceToPlayer;
private var animStart : float; // Attack
private var runStart : float;// Run
private var idleStart : float; // Idle

private var controller : CharacterController;




function Awake(){
	
	
	controller = transform.GetComponent(CharacterController);
	
	characterType = "enemy";
	
	//runSpeed = Random.Range(1.5, 4.7);	// Uncomment if you want random speed

	idleAnim = "idle";
	idleStart = GetComponent.<Animation>()[idleAnim].time;

	var playerCount : int = Network.connections.Length +1;

	var animNumber : float= Random.Range(0.6, 3.4);	// Used with mathf.Round this will give us a random number between 1-3 (walk1, walk2, etc.). Increase 3.4 to 4.4 to make runAttack a random choice too.

	if (!walkAnim){
		if (runSpeed > 3.8){ 	// If the speed is higher than 3.8 the zombie will use runAttack animation will be used
		
			walkAnim="walk";
		}
		else {
			var anim : int = Mathf.Round(animNumber);
			walkAnim = "walk";		// walk1, walk2, etc.
			runStart = GetComponent.<Animation>()[walkAnim].time;
			AudioSource.PlayClipAtPoint(randomSound, transform.position);
		}
	}

	// Same as with walk animations

	if (!attackAnim){
		var attackNumber : float= Random.Range(0.6, 3.4);
		var atck : int = Mathf.Round(attackNumber);
		attackAnim = "attack1";
		animStart = GetComponent.<Animation>()[attackAnim].time;
	
		if (GetComponent.<Animation>().IsPlaying(attackAnim)){
			GetComponent.<Animation>()[attackAnim].time = GetComponent.<Animation>()[attackAnim].time;
		}	
	
	
		GetComponent.<Animation>()[walkAnim].speed *= (runSpeed / 3);
	
		var tempTarget : Transform = GameObject.Find("Player").transform;
		if (!target){
			if (tempTarget){
			target = tempTarget;
			}
		}
	
	
	}
}	 // End Awake
	function Start(){
		
		GetComponent.<Animation>()[attackAnim].speed = 0.7;		// Adjust the speeds a little bit
		GetComponent.<Animation>()[attackAnim].layer = 2;
		GetComponent.<Animation>()[walkAnim].layer = 1;
	}
	
	// UPDATE
	function Update () {
			
			
			// I recommend you to find a premade A* pathfinding system for Unity to get some good behaviour out of these zombies.
			// Personally I've used the one on http://www.arongranberg.com . Very good!
			// Had to alter a lot of script since I could not include it in this package due to licensing.
			
		if (target) { 	// Making the zombie move towards the player IF the zombie has a target. It will also not play a walk animation unless it has a target
			var forward : Vector3 = transform.TransformDirection(Vector3.forward);		
			
			transform.LookAt(target);
			transform.rotation.x = 0;
			transform.rotation.z = 0;
			controller.SimpleMove(forward * runSpeed);
			
			// Handle walking
			if (GetComponent.<Animation>().IsPlaying(walkAnim)){
				GetComponent.<Animation>()[walkAnim].time = GetComponent.<Animation>()[walkAnim].time;
			}
			
			else {
				GetComponent.<Animation>()[walkAnim].time = runStart;
				GetComponent.<Animation>()[walkAnim].speed = 1;
				GetComponent.<Animation>().CrossFade(walkAnim);
				AudioSource.PlayClipAtPoint(attackSound, transform.position);
			}
			
			
			// Checks distance to see if an attack should be performed
			var distun = Vector3.Distance(target.position, transform.position );
			if (distun < 20){	// In this case it's 1.5 unity meters
				MeleeAttack(target);
			
			}
			
			if (distun < 30){	// In this case it's 1.5 unity meters
				//MeleeAttack(target);
				var angle :float = 30;
				if  ( Vector3.Angle(target.forward, transform.position - target.position) < angle) {
				 	target.SendMessageUpwards("AimZombie",gameObject, SendMessageOptions.DontRequireReceiver);

				}
			
			}
			
		}
		
		else {
		
			if (GetComponent.<Animation>().IsPlaying(idleAnim)){
				GetComponent.<Animation>()[idleAnim].time = GetComponent.<Animation>()[idleAnim].time;
			}
			
			else {
				GetComponent.<Animation>()[idleAnim].time = runStart;
				GetComponent.<Animation>()[idleAnim].speed = 1;
				GetComponent.<Animation>().CrossFade(idleAnim);
			}
		
		}
		
	} // end UPDATE
		
	
	
	// This one will makes zombies push objects with a "movaAble" tag
	// Comment out this to gain a bit of performance
	function OnControllerColliderHit (coll : ControllerColliderHit){
		
		if (coll.gameObject.tag == "movaAble") {
		
		var body : Rigidbody = coll.collider.attachedRigidbody;
		var pushDir : Vector3 = Vector3 (coll.moveDirection.x, 0, coll.moveDirection.z);
		body.AddForce(pushDir * 10);
		}
		
	}
	
	
	
	
	function ApplyDamage (damage : int) {
		HP -= damage;
		
		if (HP < 1){	
			target.SendMessageUpwards("KillZombie", gameObject.transform.position, SendMessageOptions.DontRequireReceiver);	
			Destroy(gameObject);
			CreateRagdoll();
		}
		
	}
	
	function SetIntro (damage : int) {
		introwalk = true;
		
	}
	
	function SetSpeed (speed : int) {
		runSpeed = speed;
		
	}
	
	
	
	function ApplyHeadshot(damage : int){
		HP -= damage;
		headHP -= damage;
		
		
		if (HP < 1){
			target.SendMessageUpwards("KillZombie", gameObject.transform.position , SendMessageOptions.DontRequireReceiver);
			Destroy(gameObject);
			CreateRagdoll();
		}
		else if (headHP < 1){
			target.SendMessageUpwards("KillZombie", gameObject.transform.position, SendMessageOptions.DontRequireReceiver);
			Destroy(gameObject);
			CreateHeadlessRagdoll();
		}
	}
	
	
	
	// Kills off zombie
	function killObject() {
		target.SendMessageUpwards("KillZombie", gameObject.transform.position, SendMessageOptions.DontRequireReceiver);
		Destroy(gameObject);
	}
	
	
	
	
	
	
	// Initiates ragdoll creation
	function CreateRagdoll(){
		var hitZed = transform;
		var dead = Instantiate(ragDoll, hitZed.position, hitZed.rotation);
		CopyTransformsRecurse(hitZed, dead);
	}
	
	function CreateHeadlessRagdoll(){
		var hitZed = transform;
		var dead = Instantiate(ragDoll_headless, hitZed.position, hitZed.rotation);
		CopyTransformsRecurse(hitZed, dead);
	}
	
	
	
	// Create ragdoll
	function CopyTransformsRecurse (src : Transform,  dst : Transform) {
		dst.position = src.position;
		dst.rotation = src.rotation;
		
		for (var child : Transform in dst) {
			
			var curSrc = src.Find(child.name);
			if (curSrc)
			CopyTransformsRecurse(curSrc, child);
		}
	}
	
	
	
	
	// Perform an attack
	
	function MeleeAttack(attackTarget : Transform){
		
		
		if (!GetComponent.<Animation>().IsPlaying(attackAnim)){ // Will be ready to attack when animation is finished
			
			if(introwalk == false)
			{
				attackTarget.SendMessageUpwards("TakeDamage", attackDmg, SendMessageOptions.DontRequireReceiver);
				AudioSource.PlayClipAtPoint(attackSound, transform.position);
				GetComponent.<Animation>()[attackAnim].time = animStart;
				
				var tempTarget : GameObject = GameObject.Find("Player");
				
				//var spawn : Player = tempTarget.GetComponent("Player"); 
				//tempTarget.GetComponent("Player") = tempTarget.GetComponent("Player").Health = attackDmg;
				GetComponent.<Animation>().CrossFade(attackAnim);
			} else {
				Destroy(gameObject);
			}
			return;
		}
		
	}
	
	
	
	
	// This one was used for making zombies attack objects together with the "interaction trigger".
	// Used by: zombie_trigger.js
	function AttackObstacle(obj : GameObject){
		
		if (!GetComponent.<Animation>().IsPlaying(attackAnim)){
			obj.SendMessageUpwards("TakeDamage", attackDmg, SendMessageOptions.DontRequireReceiver);
			GetComponent.<Animation>()[attackAnim].time = animStart;
			GetComponent.<Animation>().CrossFade(attackAnim);
			AudioSource.PlayClipAtPoint(attackSound, transform.position);
			return;
		}
		
	}
	
	
	
	@script RequireComponent(CharacterController)