using UnityEngine;
using System.Collections;


public class WeaponControl : MonoBehaviour
{
	static private Joystick[] joysticks;
	static private bool enumeratedJoysticks = false;
	static private float tapTimeDelta = 0.3f;
	public Vector2 deadZone = Vector2.zero;
	public int tapCount;
	public Vector2 position;
	public bool normalize;
	private GUITexture gui;
	private Rect defaultRect;
	private Vector2 guiTouchOffset;
	private Boundary guiBoundary = new Boundary ();
	private Vector2 guiCenter;
	private int lastFingerId = -1;
	private float tapTimeWindow;
    
	public void Start ()
	{
		gui = (GUITexture)GetComponent (typeof(GUITexture));
            
		//gui.pixelInset.width = (Screen.width/6);
		//gui.pixelInset.height = (Screen.width/6);
			
		// get where the gui texture was originally placed
		defaultRect = gui.pixelInset;
            
		// get our offset for center instead of corner;
		guiTouchOffset.x = defaultRect.width * 0.5f;
		guiTouchOffset.y = defaultRect.height * 0.5f;
            
		guiBoundary.min.x = defaultRect.x - guiTouchOffset.x;
		guiBoundary.max.x = defaultRect.x + guiTouchOffset.x;
		guiBoundary.min.y = defaultRect.y - guiTouchOffset.y;
		guiBoundary.max.y = defaultRect.y + guiTouchOffset.y;
            
		guiCenter.x = defaultRect.x + guiTouchOffset.x;
		guiCenter.y = defaultRect.y + guiTouchOffset.y;
            
	}
    
	public void Reset ()
	{
		gui.pixelInset = defaultRect;   
		lastFingerId = -1;
	}
    
	public void LatchedFinger (int fingerId)
	{
		if (lastFingerId == fingerId) {
			Reset ();
		}
	}
    
	public void Disable ()
	{
		gameObject.active = false;
		enumeratedJoysticks = false;
	}

	public void OnGUI ()
	{
		if (Input.GetMouseButton (0)) {
			//GUI.Label(new Rect(10,110,100,100), "MB0");
			
		}
		
		if (Input.GetMouseButton (1)) {
			//GUI.Label(new Rect(10,110,100,100), "MB1");
		}
		
		if (Input.GetMouseButton (2)) {
			//GUI.Label(new Rect(10,110,100,100), "MB2");
		}
		
		string mPos = "" + Input.mousePosition.x;
		//GUI.Label (new Rect (10, 10, 100, 100), mPos);
	}
    
	public void Update ()
	{       
		if (!enumeratedJoysticks) {
			joysticks = (Joystick[])FindObjectsOfType (typeof(Joystick));
			enumeratedJoysticks = true;
		}
            
		int count;
		bool mouseDown = true;
#if UNITY_IPHONE && !UNITY_EDITOR
		
             count = Input.touchCount;
#else
		
		count = Input.touchCount;
		
		if(Input.GetMouseButton(0))
		{
			count = 1;
			mouseDown = true;
		}
		else
		{
			count = 0;
			mouseDown = false;
		}
		
		if(count > 0)
		{
			//GameManager.Instance.joystick = true;
			//mouseDown = true;
		/*
				if(Input.GetMouseButton(0))
				{
					count = 1;
					mouseDown = true;
				}
				else
				{
					count = 0;
					mouseDown = false;
				}
			*/
		} else {
			//GameManager.Instance.joystick = false;
			//mouseDown = false;
		}
#endif
			
            
		if (tapTimeWindow > 0) {
			tapTimeWindow -= Time.deltaTime;
		} else {
			tapCount = 0;
		
		}
            
		// no fingers are touching, so we reset the position
		if (count == 0) {
			Reset ();        
		} else {
			int i;
			for (i = 0; i < count; i++) {
				Touch touch;
				Vector2 touchPos;
				int fingerID;
				int touchTapCount;
#if !UNITY_EDITOR

                            touch = Input.GetTouch(i);
							touchPos = touch.position;
							fingerID = touch.fingerId;
							touchTapCount = touch.tapCount;
							
#else
				touchPos = Input.mousePosition;
				fingerID = 1;
				touchTapCount = 1;
							
#endif
                            
				Vector2 guiTouchPos = touchPos - guiTouchOffset;
                            
				if (gui.HitTest (touchPos) && ((lastFingerId == -1) ||(lastFingerId != fingerID))) {       
					lastFingerId = fingerID;
                                    
					if (tapTimeWindow > 0) {
						tapCount++;     
					} else {
						tapCount = 1;
						tapTimeWindow = tapTimeDelta;
					}
                    
					foreach (Joystick j in joysticks) {
						if (j != this) {
							j.LatchedFinger (fingerID);      
						}
					} 
				}
                                            
				if (lastFingerId == fingerID) {
					if (touchTapCount > tapCount) {
						tapCount = touchTapCount;
					}
                                    
					Rect tempRect = gui.pixelInset;
					tempRect.x = Mathf.Clamp (guiTouchPos.x, guiBoundary.min.x, guiBoundary.max.x);
					tempRect.y = Mathf.Clamp (guiTouchPos.y, guiBoundary.min.y,guiBoundary.max.y);
					gui.pixelInset = tempRect;

#if UNITY_IPHONE && !UNITY_EDITOR
		
					if( touch.phase == TouchPhase.Ended 
                            || touch.phase == TouchPhase.Canceled )
                    {
                            Reset();        
                    }
#else
					
					if (!mouseDown) {
						//Reset ();
					}
#endif

					// another check to see if fingers are touching
                                    
				}
			}
		}
            
		position.x = (gui.pixelInset.x + guiTouchOffset.x - guiCenter.x) / guiTouchOffset.x;
		position.y = (gui.pixelInset.y + guiTouchOffset.y - guiCenter.y) / guiTouchOffset.y;
            
            
		float absoluteX = (float)Mathf.Abs (position.x);
		float absoluteY = (float)Mathf.Abs (position.y);
		if (absoluteX < deadZone.x) {
			position.x = 0.0f;      
		} else if (normalize) {
			position.x = Mathf.Sign (position.x) * (absoluteX - deadZone.x) / (1 - deadZone.x);
		}
            
		if (absoluteY < deadZone.y) {
			position.y = 0.0f;
		} else if (normalize) {
			position.y = Mathf.Sign (position.y) * (absoluteY - deadZone.y) / (1 - deadZone.y);
		}
		
			
	}
}