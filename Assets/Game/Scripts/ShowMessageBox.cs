using UnityEngine;
using System.Collections;

public class ShowMessageBox : MonoBehaviour {
	
	private bool showMessage = false;
	Rect windowRect = new Rect (20, 20, 120, 50);
	public string message = "Hi, This is Muscular Man! How can I help you?";
	// Use this for initialization
	void Start () 
	{
		windowRect = new Rect(Screen.width * 0.35f, Screen.height * 0.35f, Screen.width * 0.3f, Screen.height * 0.4f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
	void OnGUI() 
	{
		if (showMessage)
        	windowRect = GUI.Window(0, windowRect, DoMyWindow, "Hello!");
    }
    void DoMyWindow(int windowID) 
	{
		GUI.Label(new Rect(20, 20, Screen.width * 0.25f, Screen.height * 0.2f), message);
		
        if (GUI.Button(new Rect(Screen.width * 0.1f, Screen.height * 0.2f, Screen.width * 0.1f, Screen.height * 0.1f), "Close"))
		{
			showMessage = false;
			if (Time.timeScale == 0)
			{
				Time.timeScale = 1;
			}
		}
        
    }
	private void inputBegan()
	{
		ShowMessage();
	}
	
	public void ShowMessage()
	{
		showMessage = true;
		windowRect = new Rect(Screen.width * 0.35f, Screen.height * 0.35f, Screen.width * 0.3f, Screen.height * 0.4f);
	}
	
}
