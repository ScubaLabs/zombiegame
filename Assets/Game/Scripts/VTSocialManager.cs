using UnityEngine;
using System.Collections;

/// <summary>
/// VT social manager. Manager to control Facebook/Twitter connections for Vampire Touch
/// </summary>
public class VTSocialManager : MonoBehaviour
{
	  #region singleton class
    private static VTSocialManager instance = null;

    private VTSocialManager()
    {
    }

    public static VTSocialManager Instance
    {
        get
        {
            if (instance == null)
            {
                //  FindObjectOfType(...) returns the first VTSocialManager object in the scene.
                var gameManagerGO = GameObject.Find("VTSocialManager");
                if (gameManagerGO != null)
                    instance = gameManagerGO.GetComponent<VTSocialManager>();
            }

            // If it is still null, create a new instance
            if (instance == null)
            {
                //                Debug.Break();
                GameObject obj = new GameObject("VTSocialManager");
                instance = obj.AddComponent(typeof(VTSocialManager)) as VTSocialManager;
                Debug.Log("Could not locate VTSocialManager object. \n VTSocialManager was Generated Automatically.");
            }
            return instance;
        }
    }
    #endregion

	
	
	public string facebookAppId = "138294852910700";
	public string message = "Hi please check out my game Flash Final!";
	public Texture2D facebookIcon;
	private GUIStyle fbstyle;
			
	void Awake()
    {
        // make sure there are no duplicate social managers
        if (Instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        // set SocialManager to not be destroyed
        DontDestroyOnLoad(transform.gameObject);
	}
	
#if UNITY_IPHONE
	// Use this for initialization
	void Start () 
	{
		Debug.Log("VTSocialManager.Start");
	
		FacebookInitialization();
		
	}
#endif

#if UNITY_IPHONE
	void FacebookInitialization()
	{
		FacebookBinding.init(facebookAppId);
		FacebookManager.loginSucceededEvent += delegate()
		{
			PostMessageToFB();
		};
		FacebookManager.loginFailedEvent += delegate(string msg) 
		{
			Debug.Log("Facebook login failed: " + msg);	
		};
	}
	
#endif

	// common event handler used for all graph requests that logs the data to the console
	void completionHandler( string error, object result )
	{
		if( error != null )
			Debug.Log("Score post Unsuccessful: " + error);
		else
			Debug.Log("Score post successful");

	}
	
#if UNITY_IPHONE

	
	public void LoginAndPostFacebook()
	{
		Debug.Log("LoginAndPostFacebook");
		if (!FacebookBinding.isSessionValid())
		{
			FacebookBinding.loginWithRequestedPermissions(new string[]{"publish_stream"});
		}
		else
		{
			PostMessageToFB();
		}
	}
	
	private void PostMessageToFB()
	{
		Facebook.instance.postMessage(message, completionHandler);
	}
	
	private void OnGUI()
	{
		if (fbstyle == null)
		{
			fbstyle = new GUIStyle(GUI.skin.button);
			fbstyle.onNormal.background = facebookIcon;
			fbstyle.onHover.background = facebookIcon;
			fbstyle.hover.background = facebookIcon;
			fbstyle.normal.background = facebookIcon;
			fbstyle.active.background = facebookIcon;
		}
		if (GUI.Button(new Rect(Screen.width * 0.85f, Screen.height * 0.9f, Screen.width * 0.1f, Screen.height * 0.1f), "", fbstyle))
		{
			LoginAndPostFacebook();
		}
	}
	
#endif
}
