using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour 
{

    public static HUD instance;

    public Texture2D texHud;
	public Texture2D texMap;
	public Texture2D avatar;
	public Texture2D bullets;
    public Texture2D texHealthBarFrame;
    public Texture2D texHealthBarFill;
    public Texture2D texFlowBarFill;
	public Texture2D restBar;
	public Texture2D inventoryBack;
	public Texture2D diamondBack;
	
	public Texture2D fireSpell;
	public Texture2D iceSpell;
	public Texture2D earthSpell;
	
	private Player playerScript;
	private Transform playerTransform;

	
    public Font hudFont;
	
	public bool inventory = false;
	public bool shooting = false;
	
	public GUIStyle customButton; 
	public GUIStyle imageButton; 

    private float healthPercentage = 67;
    private float flowPercentage = 0;
	private float restPercentage = 0;
	
	public float hudheight;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () 
    {
		GameObject temp = GameObject.FindGameObjectWithTag("Player");
		if(temp != null)
		{
			playerTransform = temp.transform;
			playerScript = temp.GetComponent<Player>();
		}
	}
	
	// Update is called once per frame
	void Update () 
    {
		
        if (UI.instance != null)
        {
            SetFlowPercentage(100);
        }
				
	}

    void OnGUI()
    {
		//GUISizer.BeginGUI();
        float x, y, width, height;
        x = 0;
        y = 0;
        width = Screen.width;
        hudheight = width / 5.676f;
		height = width / 5.676f;
        //GUI.DrawTexture(new Rect(x, y, width, hudheight), texHud);
		
		x = 0;
        y = 0;
		
		
		//GUI.DrawTexture(new Rect(x, y, 324, 241), texMap);
		
		
		x = 10;
        //x = width * 0.23476f;
        y = height * 0.05925f;
        height = height * 0.17f;
        width = 200;
        //GUI.DrawTexture(new Rect(x, y, width, height), texHealthBarFrame);


		float ax = 10;
        float ay = 10;
        float aheight = 207;
        float awidth = 424;
        //GUI.DrawTexture(new Rect(ax, ay, awidth, aheight), avatar);

        x = 10;
        y = y + 15;
        width = width - 6;
        height = 20;
        width = 200 * (healthPercentage / 100);
        GUI.DrawTexture(new Rect(x, y, width, height), texHealthBarFill);

        x = x + x * 0.155f;
        
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.white;
        style.font = hudFont;
		style.fontSize = 25;
        style.alignment = TextAnchor.UpperLeft;
        GUI.Label(new Rect(x, y-5, width, height), "Health : " + healthPercentage.ToString() + "%", style);

       

		/*
		x = 10;
       // x = width * 0.23476f;
        y = height * 0.25f;
        height = height * 0.17f;
        width = 200;
        //GUI.DrawTexture(new Rect(x, y, width, height), texHealthBarFrame);
		*/
        x = 10;
        y = y+25;
        height = 20;
        width = width * (flowPercentage / 100);
        GUI.DrawTexture(new Rect(x, y, width, height), texFlowBarFill);

		x = x + x * 0.155f;
		width = Screen.width;
		style = new GUIStyle();
		style.normal.textColor = Color.white;
		style.font = hudFont;
		style.fontSize = 25;
		style.alignment = TextAnchor.UpperLeft;
		GUI.Label(new Rect(x, y-5, width, height), "Flow : " + flowPercentage.ToString() + "%", style);


		x = 10;
		y = y+25;
		width = 200;
		height = 5;
		width = width * (restPercentage / 100);
		GUI.DrawTexture(new Rect(x, y, width, height), restBar);
		
		/*
		style = new GUIStyle();
        style.normal.textColor = Color.white;
        style.font = hudFont;
		style.fontSize = 100;
        style.alignment = TextAnchor.UpperLeft;
		GUI.Label(new Rect(Screen.width-125, -10, 100, 25),GameManager.Instance.ammo.ToString(), style);


		style = new GUIStyle();
        style.normal.textColor = Color.white;
        style.font = hudFont;
		style.fontSize = 100;
        style.alignment = TextAnchor.UpperLeft;
		GUI.Label(new Rect(Screen.width-125, 70, 100, 25),GameManager.Instance.zombiesdead.ToString(), style);
		*/
        

		if (UI.instance != null)
        {
            
              //  GUI.Box(new Rect(280, 100, Screen.width - 350, Screen.height - 200), new GUIContent("Door Status"));
				//LevelManager.Instance.SetToNextLevel();
            
        }
		/*
		if(GUI.Button (new Rect ( 250,hudheight,150,20), "MENU", customButton))
		{
			

		}
		if(GUI.Button (new Rect ( 250 + 188 + 25,hudheight,150,20), "INVENTORY", customButton))
		{
			Debug.Log("Clicked the button with text");
			if(inventory == true)
			{
				inventory = false;
			} else {
				inventory = true;
			}
		}
		*/

	
		/*

		
		
		if(GUI.RepeatButton (new Rect ( Screen.width-(buttonWidth*2),Screen.height-buttonWidth,buttonWidth,buttonWidth), iceSpell, imageButton))
		{
				GameObject temp = GameObject.FindGameObjectWithTag("Player");
        		
			
			if(GameManager.Instance.selectedEnemy != null)
			{
				Vector3 targetPos = GameManager.Instance.selectedEnemy.transform.position;
				GameObject spell = Instantiate(UnityEngine.Resources.Load("Prefabs/Spells/Ice Hit")) as GameObject;
				Vector3 newPos = new Vector3(targetPos.x, targetPos.y + 5, targetPos.z);
				spell.transform.position = newPos;
				GameManager.Instance.selectedEnemy.GetComponent<Enemy>().Die();
				playerScript.Flow = playerScript.Flow - 10;
				HUD.instance.SetFlowPercentage(playerScript.Flow);
			} else {
	
			}

		}
		
		if(GUI.RepeatButton (new Rect ( Screen.width-(buttonWidth*3),Screen.height-(buttonWidth*2),buttonWidth,buttonWidth), fireSpell, imageButton))
		{
				GameObject temp = GameObject.FindGameObjectWithTag("Player");
        		GameObject spell = Instantiate(UnityEngine.Resources.Load("Prefabs/Spells/Fire Hit")) as GameObject;
				Vector3 newPos = new Vector3(temp.transform.position.x, temp.transform.position.y + 5, temp.transform.position.z);
				spell.transform.position = newPos;
			
				playerScript.Flow = playerScript.Flow - 10;
				HUD.instance.SetFlowPercentage(playerScript.Flow);
			
		}
		

		//*/
		
		//if(inventory == true)
		//{
		//	GUI.Box (new Rect ( 0,0,Screen.width,Screen.height), "");
		// 	GUI.DrawTexture(new Rect(Screen.width* 0.125f, Screen.height* 0.125f, Screen.width* 0.75f, Screen.height* 0.75f), inventoryBack);
			
		//	if(GUI.Button (new Rect ( Screen.width* 0.75f,125,150,20), "Close"))
		//	{
		//		Debug.Log("Clicked the button with text");
		//		if(inventory == true)
		//		{
		//			inventory = false;
		//		} else {
		//			inventory = true;
		//		}
		//	}
		//}
		
	
	}

    public void SetHealthPercentage(float p)
    {
        if (p < 0)
        {
            healthPercentage = 0;
        }
        else if (p > 100)
        {
            healthPercentage = 100;
        }
        else
        {
			healthPercentage = Mathf.Round (p);
        }
    }

    public void SetFlowPercentage(float p)
    {
        if (p < 0)
        {
            flowPercentage = 0;
        }
        else if (p > 100)
        {
            flowPercentage = 100;
        }
        else
        {
			flowPercentage = Mathf.Round (p);
        }
    }

	public void SetRestPercentage(float p)
	{
		if (p < 0)
		{
			restPercentage = 0;
		}
		else if (p > 100)
		{
			restPercentage = 100;
		}
		else
		{
			restPercentage = Mathf.Round (p);
		}
	}
}
