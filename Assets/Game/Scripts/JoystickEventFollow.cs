using UnityEngine;
using System.Collections;

/// <summary>
/// Joystick event
/// </summary>
public class JoystickEventFollow : MonoBehaviour {

	public float xAxis = 0;
	public float yAxis = 0;
	public float speed = 30;
	private Transform thisTransform;
	private CharacterController character;
	public float jumpSpeed = 8;
	public float inAirMultiplier = 0.25f; 				// Limiter for ground speed while jumping
	public float rotationSpeed = 5;	// Camera rotation speed for each axis
	public float protationSpeed = 300f;	// Camera rotation speed for each axis

	private Vector3 velocity; // Used for continuing momentum while in air
	private Vector3 prevForward;
	public Vector3 direction;
	public Vector3 moveCalc;





	void Start()
	{

		// Cache component lookup at startup instead of doing this every frame	
		thisTransform = GetComponent<Transform>(  );
		character = GetComponent<CharacterController>(  );	

	}
	
	void OnGUI()
	{
		//GUI.Label(new Rect(300,300,100,100), xAxis.ToString());
		//GUI.Label(new Rect(300,350,100,100), yAxis.ToString());
		//GUI.Label(new Rect(300,400,100,100), moveCalc.ToString());
		//GUI.Label(new Rect(300,450,100,100), direction.ToString());
	}

	void Update()
	{
		Vector3 movement = thisTransform.TransformDirection( new Vector3( xAxis, 0, yAxis ) );

		// Let's use the largest component of the joystick position for the speed.
		Vector2 absJoyPos = new Vector2( Mathf.Abs( xAxis ), Mathf.Abs( yAxis ) );
		movement *= absJoyPos.y ;
		movement *= speed;
		Player.instance.transform.Rotate(0, xAxis*rotationSpeed,0); //rotate attached GameObject

		character.Move(Player.instance.GetMovement(movement));
		//Player.instance.FaceMovementDirection();
		if(xAxis == 0 && yAxis == 0){
			if(Player.instance.attacking == true)
			{
				GetComponent<Animation>().CrossFade("spell");
			} else {
				GetComponent<Animation>().CrossFade("idle");
			}
		}
	}
	
	void OnEnable(){
		EasyJoystick.On_JoystickMove += On_JoystickMove;
		EasyJoystick.On_JoystickMoveEnd += On_JoystickMoveEnd;
	}
	
	void OnDisable(){
		EasyJoystick.On_JoystickMove -= On_JoystickMove	;
		EasyJoystick.On_JoystickMoveEnd -= On_JoystickMoveEnd;
	}
		
	void OnDestroy(){
		EasyJoystick.On_JoystickMove -= On_JoystickMove;	
		EasyJoystick.On_JoystickMoveEnd -= On_JoystickMoveEnd;
	}
	
	void On_JoystickMoveEnd( MovingJoystick move){
		if (move.joystickName == "movePad"){
			xAxis = 0;
			yAxis = 0;
			GetComponent<Animation>().CrossFade("idle");
		}
	}
	void On_JoystickMove( MovingJoystick move){

		if (move.joystickName == "movePad"){

			xAxis = move.joystickAxis.x;
			yAxis = move.joystickAxis.y;
			

			if(HUD.instance.shooting == true)
			{
				GetComponent<Animation>().CrossFade("spell");
			} else if (Player.instance.useflow)
			{
				GetComponent<Animation>().CrossFade("idle");
			} else {
				
				var yrun = false;
				var xrun = false;
				if ((Mathf.Abs(yAxis) < 0.5) && (Mathf.Abs(yAxis) > -0.5)){
					GetComponent<Animation>().CrossFade("walk");
					yrun = false;
				} else {
					GetComponent<Animation>().CrossFade("run");
					yrun = true;
				}
				
				if ((Mathf.Abs(xAxis) < 0.5) && (Mathf.Abs(xAxis) > -0.5)){
					GetComponent<Animation>().CrossFade("walk");
					xrun = false;
					
				} else {
					GetComponent<Animation>().CrossFade("run");
					xrun = true;
				}
				
				if(yrun == true || xrun == true)
				{
					GetComponent<Animation>().CrossFade("run");
				}
				
				if ((Mathf.Abs(yAxis) == 0) && (Mathf.Abs(xAxis) == 0)){
					GetComponent<Animation>().CrossFade("idle");
				}

				//direction = new Vector3(0, 0, yAxis);
				//character.Move(Player.instance.GetMovement(movement));
				//Player.instance.FaceMovementDirection();

				if (Player.instance.CheckVelocity())
				{
					GameManager.Instance.selectedEnemy = null;
				}

			}
		}
	}
	
}
