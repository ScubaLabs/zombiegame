using UnityEngine;
 using System;
 using System.Collections;
 using System.Collections.Generic;

public class Player : MonoBehaviour 
{
	public static Player instance;

	private Transform thisTransform;

	public CharacterController character;

	public float jumpSpeed = 8;

	public float inAirMultiplier = 0.25f; 				// Limiter for ground speed while jumping

	public Vector2 rotationSpeed = new Vector2( 5, 5 );	// Camera rotation speed for each axis

	public float protationSpeed = 300f;	// Camera rotation speed for each axis
	
	private Vector3 velocity; // Used for continuing momentum while in air

	private Vector3 prevForward;

	public Vector3 direction;

	public Vector3 moveCalc;
	
	private float health;
	
	private float flow;

	public float pfloor;
	
	private float strength;

	private float rest;
	
	private float defense;

	public float speed = 30;

	public bool useflow = false;

	public bool attacking = false;

	private bool floatup;

	private bool shooting = false;

	public LineRenderer lineOne;

	public GameObject healthPrefab;

	public GameObject bloodEmitter; 

	public GameObject wings;

	public Vector3 ragpos;


	public float Health
	{
		get {return health;}
		set 
		{
			if(value > 100)
			{
				health = 100;
			}
			else if(value < 0)
			{
				health = 0;
			}
			else
			{
				health = value;
			}
			
		}
	}
	
	public float Flow
	{
		get {return flow;}
		set 
		{
			if(value > 100)
			{
				flow = 100;
			}
			else if(value < 0)
			{
				flow = 0;
			}
			else
			{
				flow = value;
			}
			
		}
	}

	public float Rest
	{
		get {return rest;}
		set 
		{
			if(value > 100)
			{
				rest = 100;
			}
			else if(value < 0)
			{
				rest = 0;
			}
			else
			{
				rest = value;
			}
			
		}
	}
	
	public Weapon gun;

	void Awake()
	{
		instance = this;
		floatup = false;
		thisTransform = GetComponent<Transform>(  );
		character = GetComponent<CharacterController>(  );	
		lineOne = GetComponent<LineRenderer>();


	}
	
	// Use this for initialization
	void Start () 
	{
		health = 100;
		flow = 0;
		rest = 100;
		strength = 10;
		defense = 10;
		pfloor = 0;
		GameManager.Instance.ammo = 300;
 
	    lineOne.enabled = false;
	 
	    lineOne.SetVertexCount(2);
	 
	    lineOne.SetWidth(0.25f, 2f);

		if(HUD.instance)
		{
			HUD.instance.SetHealthPercentage(health);
			HUD.instance.SetFlowPercentage(flow);
			HUD.instance.SetRestPercentage(rest);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		//flow = Mathf.Round (flow);
		//health = Mathf.Round (health);
		HUD.instance.SetHealthPercentage (health);
		HUD.instance.SetFlowPercentage (flow);
		HUD.instance.SetRestPercentage(rest);
		if (health <= 0) {
				Time.timeScale = 0;
		}

		if (useflow == true) {
			GameManager.Instance.selectedEnemy = null;

			pfloor = 10;
			if (flow > 0) {
				flow = flow - 0.05f;
			}
			
			if (flow < 0) {
				flow = 0;
				useflow = false;
			}
		} else {
			GameManager.Instance.selectedPortal = null;
			lineOne.enabled = false;
		
		}

		if (shooting == false) 
		{
			Rest = Rest + 0.15f;
		}

		//Debug.Log("NONE"+Rest);

	
	}
	
	void OnGUI()
	{
		if(health <= 0)
		{
			if(GUI.Button(new Rect(Screen.width/2-40, Screen.height/2-15, 80, 30), "Restart"))
			{
				Time.timeScale = 1;
				Restart();
            }
			
		}
        foreach(KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(kcode))
                Debug.Log("KeyCode down: " + kcode);
        }
//		int i = 0;
//		while (i < GameManager.Instance.floors.Length)
//		{
//			int mult = (i * 20);
//			GUI.Label(new Rect(100,100+mult,100,100),  GameManager.Instance.floors[i].transform.position.ToString());
//		}
		if (GameManager.Instance.selectedEnemy != null) {
			//GUI.Label(new Rect(100,260,200,100), "Angle:"+ Vector3.Angle(thisTransform.forward, GameManager.Instance.selectedEnemy.transform.position - thisTransform.position).ToString());
				}
		//GUI.Label(new Rect(100,290,200,100),  character.velocity.ToString());
		GUI.Label(new Rect(100,300,100,100),  thisTransform.position.ToString());
		//GUI.Label(new Rect(100,300,200,100),  cityBlock.renderer.bounds.size.ToString());
	}

	public void Restart()
	{
		health = 100;
		HUD.instance.SetHealthPercentage(health);
	}
	
	public void TakeDamage(int damage)
	{
		health = health - damage;
		HUD.instance.SetHealthPercentage(health);
		GameObject blood = Instantiate(bloodEmitter) as GameObject;
		var bpos = thisTransform.position;
		bpos.y = bpos.y + 5;
		blood.transform.position = bpos;
	}

	public void UseFlow()
	{
		///*
		if (flow > 0 && useflow == false) {
			useflow = true;
		} else {
			useflow = false;
		}
		//*/

		//GameObject spell = Instantiate (UnityEngine.Resources.Load ("Magic/_Prefab/GroundFX_Lightning01")) as GameObject;
		//var pos = thisTransform.position;
		//pos.y += 5;
		//spell.transform.position = pos;
	}
	
	public void KillZombie(Vector3 spawnpos)
	{
		GameManager.Instance.zombiesdead += 1;
		flow = flow + 10;
		HUD.instance.SetFlowPercentage(flow);

		if(UnityEngine.Random.Range(1, 7) < 3)
		{
			GameObject bug = GameObject.Instantiate(healthPrefab, spawnpos, Quaternion.identity) as GameObject;
		}
	}

	public void AimZombie(GameObject spawnpos)
	{
		if (useflow == false) 
		{
			if (GameManager.Instance.selectedEnemy == null && !CheckVelocity ()) {
					GameManager.Instance.selectedEnemy = spawnpos;
			}
		}
	}

	public void On_TouchStart(Gesture gesture){
		// Verification
		//health = 0;
		if (gesture.pickObject) 
		{
			GameObject selected = gesture.pickObject;
			if(selected.layer == LayerMask.NameToLayer("Enemies"))
			{
				GameManager.Instance.selectedEnemy = gesture.pickObject;
			}

			if(selected.layer == LayerMask.NameToLayer("Portals"))
			{
				GameManager.Instance.selectedPortal = gesture.pickObject;
			}
		}
	}

	public bool CheckVelocity()
	{
		var x = character.velocity.x;
		var z = character.velocity.z;
		if (x < -18 || x > 18 || z < -18 || z > 18) 
		{
			return true;
		} else {
			return false;
		}
	}

	
	public void FireWeapon()
	{
		attacking = true;
		if(useflow == true)
		{
			//Shooting Lasers
			if(GameManager.Instance.selectedPortal != null)
			{
				var playerpos = transform.Find ("Gun").transform.position;
				//playerpos.y += 3;

				var enemypos = GameManager.Instance.selectedPortal.transform.position;
				enemypos.y += 3;

				lineOne.enabled = true;
				lineOne.SetPosition(0, playerpos);
				lineOne.SetPosition(1, enemypos);
			}
		}  else {
			shooting = true;
			if(Rest > 0)
			{
				Rest -= 1;
                //Debug.Log("Selected Enemy not null:" + GameManager.Instance.selectedEnemy);
                Vector3 targetPos = GameManager.Instance.player.transform.position;
				//transform.LookAt(new Vector3(targetPos.x, transform.position.y, targetPos.z));
				gun.Fire(targetPos);
			}
		}

	}

	public void ReleaseWeapon()
	{
		lineOne.enabled = false;
		shooting = false;
		attacking = false;
	}

	public void FaceMovementDirection()
	{	
		var distance = 20;
		// the height we want the camera to be above the target
		var height = 20.0;
		// How much we 
		var heightDamping = 10.0;
		var rotationDamping = 2.0;

		///*
		Vector3 horizontalVelocity = character.velocity;
		horizontalVelocity.y = 0; // Ignore vertical movement

		if(GameManager.Instance.selectedEnemy != null || GameManager.Instance.selectedPortal != null)
		{
			GameObject selected = null;
			if(GameManager.Instance.selectedEnemy != null)
			{
				selected = GameManager.Instance.selectedEnemy;
			} else {
				selected = GameManager.Instance.selectedPortal;
			}
			//Debug.Log("Selected Enemy not null:" + GameManager.Instance.selectedEnemy);
			Vector3 targetPos = selected.transform.position;
			thisTransform.LookAt(new Vector3(targetPos.x, targetPos.y, targetPos.z));
		} else {
			//thisTransform.LookAt(thisTransform.position + 10*direction); //look the right way
			
			if(horizontalVelocity.magnitude > 0.1)
				thisTransform.forward = horizontalVelocity.normalized;
			
		}
		
	}

	public Vector3 GetMovement(Vector3 direction)
	{

		if (useflow == true) {
			moveCalc = direction;
			//wings.transform.Find ("Core").GetComponent<Renderer>().enabled  = true;
			//wings.transform.Find ("Glow").GetComponent<Renderer>().enabled  = true;
			wings.transform.Find ("AngelWingLeft").GetComponent<Renderer>().enabled  = true;
			wings.transform.Find ("AngelWingRight").GetComponent<Renderer>().enabled  = true;
			wings.GetComponent<Animation>().CrossFade("flap");
			///*
			if(floatup)
				moveCalc = floatingup(moveCalc);
			else if(!floatup)
				moveCalc = floatingdown(moveCalc);
			//*/
		} else {
			floatup = false;
			//wings.transform.Find ("Core").GetComponent<Renderer>().enabled  = false;
			//wings.transform.Find ("Glow").GetComponent<Renderer>().enabled  = false;
			wings.transform.Find ("AngelWingLeft").GetComponent<Renderer>().enabled  = false;
			wings.transform.Find ("AngelWingRight").GetComponent<Renderer>().enabled  = false;
			//wings.animation.CrossFade("flap");
			moveCalc = direction * speed * Time.deltaTime + Physics.gravity;
		}

		return moveCalc;
	}
	///*
	public Vector3 floatingup(Vector3 direction)
	{

		var fdirection = direction * speed * Time.deltaTime - (Physics.gravity/120);
			//character.Move (fdirection);
		if (thisTransform.position.y > 15) {
			floatup = false;
		}

		return fdirection;
	}

	public Vector3 floatingdown(Vector3 direction)
	{
		var fdirection = direction * speed * Time.deltaTime + (Physics.gravity/120);
		//character.Move (fdirection);
		//floatup = true;
		if (thisTransform.position.y < 14) {
			floatup = true;
		}

		return fdirection;
	}
	//*/

}
