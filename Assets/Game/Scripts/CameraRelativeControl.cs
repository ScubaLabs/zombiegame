using UnityEngine;
using System.Collections;


//////////////////////////////////////////////////////////////
// CameraRelativeControl.cs
// Penelope iPhone Tutorial
//
// CameraRelativeControl creates a control scheme similar to what
// might be found in 3rd person platformer games found on consoles.
// The left stick is used to move the character, and the right
// stick is used to rotate the camera around the character.
// A quick double-tap on the right joystick will make the 
// character jump.
//////////////////////////////////////////////////////////////

// This script must be attached to a GameObject that has a CharacterController
//@script RequireComponent( CharacterController )

public class CameraRelativeControl : MonoBehaviour
{
	//public EasyJoystick moveJoystick;
				// The actual transform of the camera
	
	public float speed = 30;
	// Ground speed

	public float xAxis = 0;
	public float yAxis = 0;
	public string movingText = "loaded";

	public Vector3 moveCalc;
	public float jumpSpeed = 8;
	public float inAirMultiplier = 0.25f; 				// Limiter for ground speed while jumping
	public Vector2 rotationSpeed = new Vector2( 5, 5 );	// Camera rotation speed for each axis

	
	private Transform thisTransform;
	private CharacterController character;
	private Vector3 velocity; // Used for continuing momentum while in air
	private Vector3 prevForward;
	public Vector3 direction;
	void Start()
	{
		
		var distance = 20;
		// the height we want the camera to be above the target
		var height = 20.0;
		// How much we 
		var heightDamping = 10.0;
		var rotationDamping = 2.0;
		
		
		// Cache component lookup at startup instead of doing this every frame	
		thisTransform = GetComponent<Transform>(  );
		character = GetComponent<CharacterController>(  );	

	}
	
	void FaceMovementDirection()
	{	
		var distance = 20;
		// the height we want the camera to be above the target
		var height = 20.0;
		// How much we 
		var heightDamping = 10.0;
		var rotationDamping = 2.0;

		///*
		Vector3 horizontalVelocity = character.velocity;
		horizontalVelocity.y = 0; // Ignore vertical movement
//		
//		// If moving significantly in a new direction, point that character in that direction
//		Debug.Log("dot product:" + Vector3.Dot (thisTransform.forward, prevForward));
//		if ( Vector3.Dot (horizontalVelocity.normalized, prevForward) < 0.5f )
//		{
//			prevForward = thisTransform.forward;
//			thisTransform.forward = horizontalVelocity.normalized;
//		}
//		thisTransform.forward = Vector3.Lerp(thisTransform.forward, horizontalVelocity.normalized, 0.25f);

		
		if(GameManager.Instance.selectedEnemy != null)
		{
//			Debug.Log("Selected Enemy not null:" + GameManager.Instance.selectedEnemy);
			//Vector3 targetPos = GameManager.Instance.selectedEnemy.transform.position;
			//thisTransform.LookAt(new Vector3(targetPos.x, thisTransform.position.y, targetPos.z));
		} else {
			//thisTransform.LookAt(thisTransform.position + 10*direction); //look the right way

			if(horizontalVelocity.magnitude > 0.1)
				thisTransform.forward = horizontalVelocity.normalized;

		}
	
	}

	void Update()
	{

		if (Input.GetAxis ("Vertical") != 0 || Input.GetAxis ("Horizontal") != 0) {

			/*
			if ((Input.GetAxis ("Vertical") > 0)) {
					character.Move (thisTransform.forward * speed * Time.deltaTime + Physics.gravity);

			}

			//////////WALKING BACK
			if ((Input.GetAxis ("Vertical") < 0)) {
					character.Move (thisTransform.forward * -speed * Time.deltaTime + Physics.gravity);

			}

			//////////ROTATING RIGHT
			if ((Input.GetAxis ("Horizontal") > 0)) {
					character.transform.Rotate (0, character.transform.rotation.y + 00002f, 0);
					//direction = new Vector3(0, Input.GetAxis("Horizontal"), 0);


			}

			//////////ROTATING LEFT
			if ((Input.GetAxis ("Horizontal") < 0)) {
					character.transform.Rotate (0, character.transform.rotation.y - 00002f, 0);
					//direction = new Vector3(0, Input.GetAxis("Horizontal"), 0);


			}


			//thisTransform.LookAt(direction); //look the right way
			direction = new Vector3(xAxis, 0, yAxis);
			character.Move(direction * speed * Time.deltaTime + Physics.gravity);
			FaceMovementDirection();
			*/

		} else {
//			direction = new Vector3(xAxis, 0, yAxis);
//			character.Move(direction * speed * Time.deltaTime + Physics.gravity);
//			FaceMovementDirection();
		}
	}
		
}
