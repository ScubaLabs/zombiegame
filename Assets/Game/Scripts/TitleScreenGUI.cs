using UnityEngine;
using System.Collections;

public class TitleScreenGUI : MonoBehaviour 
{

    public static TitleScreenGUI instance;

    public Texture2D titleHud;

	private Player playerScript;
	private Transform playerTransform;

	
    public Font hudFont;
	
	public bool inventory = false;
	
	public GUIStyle customButton; 
	public GUIStyle imageButton; 


	
	public float hudheight;

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () 
    {
		GameObject temp = GameObject.FindGameObjectWithTag("Player");
		if(temp != null)
		{
			playerTransform = temp.transform;
			playerScript = temp.GetComponent<Player>();
		}
	}
	
	// Update is called once per frame
	void Update () 
    {
     
				
	}

    void OnGUI()
    {

		float diamondWidth = (Screen.width/6);
		float diamondHeight = (Screen.height/3);
		float buttonWidth = (Screen.width/6)/2;
		///*
		if(GUI.Button(new Rect((Screen.width/2)-(500/2)-100, (Screen.height/2) - (430/2) -40, 500, 430), titleHud,imageButton))
		{
			Application.LoadLevel("Room1");
		}
		//*/
		/*
		if(GUI.RepeatButton(new Rect ( Screen.width-(buttonWidth*2),Screen.height-(buttonWidth*3),buttonWidth,buttonWidth), earthSpell, imageButton))
		{
				GameObject temp = GameObject.FindGameObjectWithTag("Player");
        		GameObject spell = Instantiate(UnityEngine.Resources.Load("Prefabs/Spells/Dust Attack")) as GameObject;
				Vector3 newPos = new Vector3(temp.transform.position.x, temp.transform.position.y + 5, temp.transform.position.z);
				spell.transform.position = newPos;
			
				playerScript.Flow = playerScript.Flow - 10;
				HUD.instance.SetFlowPercentage(playerScript.Flow);

		}
		
		
		if(GUI.RepeatButton (new Rect ( Screen.width-(buttonWidth*2),Screen.height-buttonWidth,buttonWidth,buttonWidth), iceSpell, imageButton))
		{
				GameObject temp = GameObject.FindGameObjectWithTag("Player");
        		
			
			if(GameManager.Instance.selectedEnemy != null)
			{
				Vector3 targetPos = GameManager.Instance.selectedEnemy.transform.position;
				GameObject spell = Instantiate(UnityEngine.Resources.Load("Prefabs/Spells/Ice Hit")) as GameObject;
				Vector3 newPos = new Vector3(targetPos.x, targetPos.y + 5, targetPos.z);
				spell.transform.position = newPos;
				GameManager.Instance.selectedEnemy.GetComponent<Enemy>().Die();
				playerScript.Flow = playerScript.Flow - 10;
				HUD.instance.SetFlowPercentage(playerScript.Flow);
			} else {
	
			}

		}
		
		if(GUI.RepeatButton (new Rect ( Screen.width-(buttonWidth*3),Screen.height-(buttonWidth*2),buttonWidth,buttonWidth), fireSpell, imageButton))
		{
				GameObject temp = GameObject.FindGameObjectWithTag("Player");
        		GameObject spell = Instantiate(UnityEngine.Resources.Load("Prefabs/Spells/Fire Hit")) as GameObject;
				Vector3 newPos = new Vector3(temp.transform.position.x, temp.transform.position.y + 5, temp.transform.position.z);
				spell.transform.position = newPos;
			
				playerScript.Flow = playerScript.Flow - 10;
				HUD.instance.SetFlowPercentage(playerScript.Flow);
			
		}
		
		if(GUI.RepeatButton (new Rect( Screen.width-buttonWidth, Screen.height-(buttonWidth*2), buttonWidth,buttonWidth), fireSpell, imageButton))
		{
		
				//if (GameManager.Instance.selectedEnemy != null)
                //{
                    GameManager.Instance.player.FireWeapon();
                //} else {
					//playerScript.Flow = playerScript.Flow + 5;
					//HUD.instance.SetFlowPercentage(playerScript.Flow);
				//}
		}
		//*/
		
	
		
	
	}


}
