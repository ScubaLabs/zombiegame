using UnityEngine;
using System.Collections;

public class FadeOutTexture : MonoBehaviour {
	
	private Color alphaColor;
	public Texture2D textureToShow;
	public Rect rectRelativeToScreen = new Rect(0.45f, 0.25f, 0.1f, 0.1f);
	// Use this for initialization
	void Start () 
	{
		alphaColor = GUI.color;
		rectRelativeToScreen.x *= Screen.width;
		rectRelativeToScreen.width *= Screen.width;
		rectRelativeToScreen.y *= Screen.height;
		rectRelativeToScreen.height *= Screen.height;
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		alphaColor.a -= 0.005f;

        if (GUI.color.a < 0.6f)
        {
            CancelInvoke("ReduceAlphaToZeroAndStop");
            alphaColor.a = 1.0f;
			GameObject.Destroy(this.gameObject);
        }
	}
	
	void OnGUI()
	{
		// Show a message if there is one to display
        GUI.color = alphaColor;
        
        GUI.DrawTexture(rectRelativeToScreen, textureToShow);
	
	}
}
