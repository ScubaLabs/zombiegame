using UnityEngine;
using System.Collections;
using System.IO;

public class TitleManager : MonoBehaviour {
	
	#region singleton class
	
	private static TitleManager instance;

	public AsyncOperation async;

	public Texture2D emptyProgressBar; // Set this in inspector.
	public Texture2D fullProgressBar; // Set this in inspector.
	
	public static TitleManager Instance
	{
		get
		{
			if(instance == null)
			{
				GameObject obj = GameObject.Find("TitleManager");
				if(obj != null)
				{
					instance = obj.GetComponent<TitleManager>();
				}
			}
			
			return instance;
		}
	}
	
	#endregion
	
	
	
	
#if UNITY_EDITOR
   // public static string appPath = Application.persistentDataPath + "/StreamingAssets";
#elif UNITY_IPHONE
	public static string appPath = GetiPhoneDocumentsPath();
#elif UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
	public static string appPath = Application.dataPath + "/StreamingAssets";
#endif
	

	 
	public Enemy selectedEnemy;
	
	public Player player;
	
	public bool intro = true;
	
	public GameObject floor;
	
	public int zombiesdead = 0;
	
	

	void Awake()
	{
		
#if UNITY_IPHONE
		
		if ( Application.platform != RuntimePlatform.OSXEditor)
		{
			var info = new DirectoryInfo(Application.dataPath + "/Raw");
			var fileInfo = info.GetFiles();
		
			
			foreach( FileInfo file in fileInfo)
			{
				if (!System.IO.File.Exists(GetiPhoneDocumentsPath() + "/" + file.Name))
				{
				System.IO.File.Copy(Application.dataPath + "/Raw/" + file.Name,GetiPhoneDocumentsPath() + "/" + file.Name);
				Debug.Log("Copying : From : " + Application.dataPath + "/Raw" + file.Name + ", To : " +GetiPhoneDocumentsPath() );
				}
			}
		}
#endif
	}
	
	void Start () {
		player = GameObject.Find("Player").GetComponent<Player>();
		//SMGameEnvironment.Instance.SceneManager.TransitionPrefab = "Transitions/SMPixelateTransition";

	}

	void OnGUI() {
		if (async != null) {
			//GUI.DrawTexture(new Rect((Screen.width/3), (Screen.height/3), 100, 50), emptyProgressBar);
			GUI.DrawTexture(new Rect((Screen.width/2), (Screen.height/2), 200 * async.progress, 50), fullProgressBar);
			GUI.skin.label.alignment = TextAnchor.MiddleCenter;
			GUI.Label(new Rect((Screen.width/2), (Screen.height/2), 100, 50),  string.Format("{0:N0}%", async.progress * 100f));
		}
	}
	
	public static string GetiPhoneDocumentsPath () 
	{
 
            // Your game has read+write access to /var/mobile/Applications/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/Documents

             // Application.dataPath returns              

             // /var/mobile/Applications/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/myappname.app/Data

             // Strip "/Data" from path

             string path = Application.dataPath.Substring (0, Application.dataPath.Length - 5);

             // Strip application name

             path = path.Substring(0, path.LastIndexOf('/'));  

             return path + "/Documents";
 
      }
	
}
