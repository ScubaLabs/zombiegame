using UnityEngine;
using System.Collections;
using System.IO;

public class GameManager : MonoBehaviour {
	
	#region singleton class
	
	private static GameManager instance;
	
	public bool intro = false;
	
	public static GameManager Instance
	{
		get
		{
			if(instance == null)
			{
				GameObject obj = GameObject.Find("GameManager");
				if(obj != null)
				{
					instance = obj.GetComponent<GameManager>();
				}
			}
			
			return instance;
		}
	}
	
	#endregion
	
	
	

	

	 
	public GameObject selectedEnemy;
	public GameObject selectedEnemyDog;
	public GameObject selectedPortal;

	public GameObject[] floors;
	
	public Player player;

	public GameObject zone = null;
	
	public int ammo = 300;

	public Vector3 newpos;
   
    public GameObject floor;
    
    public int zombiesdead = 0;
	
	#if !UNITY_IPHONE
	
		#elif UNITY_EDITOR
			
		   // public static string appPath = Application.persistentDataPath + "/StreamingAssets";
		#elif UNITY_IPHONE
			public static string appPath = GetiPhoneDocumentsPath();
		#elif UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
			public static string appPath = Application.dataPath + "/StreamingAssets";
		#endif
				
		
	

	void Awake()
	{

		player = GameObject.Find("OVRPlayerController").GetComponent<Player>();
		floor  = Instantiate(UnityEngine.Resources.Load("Town")) as GameObject;
//		floors = new GameObject[8];
//		newpos = floor.transform.position;
//		var cityBlock = floor.transform.Find ("Block/Cube");
//		newpos.x -= cityBlock.renderer.bounds.size.x;
//		newpos.z += cityBlock.renderer.bounds.size.z;
//		int znum = 1;
//		
//		for(int i=0; i<3; i++) 
//		{	
//			for(int y=0; y<3; y++)
//			{
//				if(newpos.x != 0 && newpos.z != 0)
//				{
//					Vector3 ppos = floor.transform.position;
//					ppos.x = newpos.x + cityBlock.renderer.bounds.size.x * (i);
//					ppos.z = newpos.z - cityBlock.renderer.bounds.size.z * (y);
//					floors[i]  = Instantiate(UnityEngine.Resources.Load("Town"),ppos,floor.transform.rotation) as GameObject;
//					floors[i].name = "Zone"+znum.ToString(); 
//					znum ++;
//				}
//			}
//		}
//		
//		GameObject.Destroy (floor);
//		
		#if UNITY_IPHONE
		

		#endif
	}
	
	void Start () {

		
		#if !UNITY_IPHONE
			//GameObject joy  = GameObject.Find("movePad");
		#elif UNITY_EDITOR
			//GameObject joy  = GameObject.Find("movePad");
		#elif UNITY_IPHONE
		#elif UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
			//GameObject joy  = GameObject.Find("movePad");
		#endif
	}

	void OnGUI ()
	{
		if(zone != null)
		{
			//GUI.Label(new Rect(400,100,200,100),  zone.name.ToString());
		}
	}
	

}
