using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {
	#region singleton class
    private static LevelManager instance = null;

    private LevelManager() { }

    public static LevelManager Instance
    {
        get
        {
			if (instance == null)
			{
				//  FindObjectOfType(...) returns the first AManager object in the scene.
				instance = FindObjectOfType(typeof (LevelManager)) as LevelManager;
			}
			
			// If it is still null, create a new instance
			if (instance == null)
			{
				GameObject obj = new GameObject("LevelManager");
                instance = obj.AddComponent(typeof (LevelManager)) as LevelManager;
                Debug.Log ("Could not locate an LevelManager object. \n LevelManager was Generated Automatically.");
			}
            return instance;
        }
    }
    #endregion
	// Use this for initialization
	
	public int currentLevel = 0;
	Player playerScript;
	[System.Serializable]
	public class LevelProperty
	{
		public Color lightColor;
	}
	
	public LevelProperty[] levelProperties;
	public Light[] lightsInGame;
	
	ShowMessageBox messageBox;
	void Start () 
	{
		var playerGO = GameObject.FindGameObjectWithTag("Player");
		playerScript = playerGO.GetComponent<Player>();
		messageBox = GetComponent<ShowMessageBox>();
		UpdateLightColor();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	public void SetToNextLevel()
	{
		currentLevel++;
		Time.timeScale = 0;
		playerScript.Restart();
//		SendMessage("ShowMessage", SendMessageOptions.DontRequireReceiver);
		messageBox.message = "Master Level Reached "+ currentLevel +  " !!";
		messageBox.ShowMessage();
		UpdateLightColor();
		
		// Kill all enemies in scene
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy") as GameObject[];
		foreach(var enemy in enemies)
		{
			Debug.Log("Enemy:" + enemy);
			enemy.SendMessage("Die");	
		}
		
		GameManager.Destroy(GameManager.Instance.floor);
		
		GameManager.Instance.floor = Instantiate(UnityEngine.Resources.Load("Floor2")) as GameObject;
	}
	
	private void UpdateLightColor()
	{
		foreach(var light in lightsInGame)
		{
			//light.color = levelProperties[currentLevel].lightColor;
		}
	}
	
	private void OnGUI()
	{
		/*
		if (GUI.Button(new Rect(Screen.width * 0.8f, Screen.height * 0.2f, Screen.width * 0.2f, Screen.height * 0.1f), "Upgrade to Master Level"))
		{
			SetToNextLevel();
		}
		*/
	}
}
