using UnityEngine;
using System.Collections;
using System.Collections.Generic;


#region Input States Enum
enum inputStates
{
	NA = 1,
	Began = 2,
	Moved = 3,
	Stationary = 4,
	Ended = 5
};
#endregion

#region Device States Enum 
enum deviceStates
{
	EditorState = 1,
	IphoneState = 2
};
#endregion


public class InputManager : MonoBehaviour 
{
	#region singleton class
    private static InputManager instance = null;

    private InputManager() { }

    public static InputManager Instance
    {
        get
        {
			if (instance == null)
			{
				//  FindObjectOfType(...) returns the first AManager object in the scene.
				instance = FindObjectOfType(typeof (InputManager)) as InputManager;
			}
			
			// If it is still null, create a new instance
			if (instance == null)
			{
				GameObject obj = new GameObject("InputManager");
                instance = obj.AddComponent(typeof (InputManager)) as InputManager;
                Debug.Log ("Could not locate an AManager object. \n AManager was Generated Automatically.");
			}
            return instance;
        }
    }
    #endregion
	#region Variables
	
	#region Game Object Variables
	public  List<GameObject> touchableObjects;
	private GameObject mainCamera;
	#endregion
	
	#region Gernal Variable
	private deviceStates currentDeviceState ;
	private inputStates currentInputState ;
	private GameObject ObjectToApplyInput ;
	#endregion
	
	#region Velocity on Up Variables
	private Vector3 inputStartPosition ;
	private Vector3 inputMovePosition;
	private Vector3 inputStationaryPosition;
	private Vector3 inputEndPosition;
	#endregion
	
	#endregion

	// setting current Device State according to device
	#region Start Function
	// Use this for initialization
	void Start () 
	{
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
			// mouse inputs
			currentDeviceState = deviceStates.EditorState;
		#else
			#if UNITY_IPHONE
				// touch inputs
				currentDeviceState = deviceStates.IphoneState;
			#endif	
		#endif
		mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
	}
	#endregion
	
	#region Get InputPosition Function
	Vector3 getInputPosition()
	{
		if (currentDeviceState == deviceStates.EditorState)
		{
			return Input.mousePosition ;
		}
		else if (currentDeviceState == deviceStates.IphoneState)
		{
			if (Input.touchCount > 0 )
			{
				return Input.touches[0].position;
			}
			else
			{
				Debug.Log("There was not touch whos position to be send");
			}
		}
		return Vector3.zero ;
	}
	#endregion
	
	#region Check Object If it is to move Function
	bool checkObjectHasToMove(GameObject objectTouched) // function will check the object which is touched it is in tag list to move
	{
		return touchableObjects.Contains(objectTouched);
//		for (int i = 0; i < touchableObjects.Count ; i++)
//		{
//			if (objectTouched == touchableObjects[i])
//			{
//				return true;
//			}
//		}
//		return false;
	}
	#endregion
	
	#region Responce Input State Functions For Simple Input State Gesture
	void ResponceOnInputStateForSimpleGesture() // Responce according to the state of input 
	{
		if (currentInputState == inputStates.Began)
		{
			
			inputStartPosition = mainCamera.GetComponent<Camera>().ScreenToWorldPoint(getInputPosition());
			// began send message
			RaycastHit hitInfo = new RaycastHit();
			Ray rayThroughCamera = mainCamera.GetComponent<Camera>().ScreenPointToRay(getInputPosition());
			if (Physics.Raycast(rayThroughCamera,out hitInfo))
			{
				// call the function of moved of collider
				if (checkObjectHasToMove(hitInfo.collider.gameObject))
				{
					ObjectToApplyInput = hitInfo.collider.gameObject;
					ObjectToApplyInput.SendMessage("inputBegan",inputStartPosition,SendMessageOptions.DontRequireReceiver);
					currentInputState = inputStates.Moved ;
				}
				else
				{
//					Debug.Log("This Object no moveable");
					ObjectToApplyInput = null;
				}
			}
		}
		else if (currentInputState == inputStates.Moved)
		{
			inputMovePosition = mainCamera.GetComponent<Camera>().ScreenToWorldPoint(getInputPosition());
			// moved Send Message
			if (ObjectToApplyInput)
			{
				ObjectToApplyInput.SendMessage("inputMoved",inputMovePosition,SendMessageOptions.DontRequireReceiver);
			}
		}
		else if (currentInputState == inputStates.Stationary)
		{
			inputStationaryPosition = mainCamera.GetComponent<Camera>().ScreenToWorldPoint(getInputPosition());
			// stationary send Meesage
			if (ObjectToApplyInput)
			{
				ObjectToApplyInput.SendMessage("inputStationary",inputStationaryPosition,SendMessageOptions.DontRequireReceiver);
			}
		}
		else if (currentInputState == inputStates.Ended)
		{
			
			inputEndPosition = mainCamera.GetComponent<Camera>().ScreenToWorldPoint(getInputPosition());
			
			if (ObjectToApplyInput)
			{
				ObjectToApplyInput.SendMessage("inputEnded",inputEndPosition,SendMessageOptions.DontRequireReceiver);
			}
			currentInputState = inputStates.NA;
			
		}
	}
	#endregion
	
	#region Assign Input State On input according to Device (Mouse Click/Touch)
	void AssignInputState() // Assign input State according to input device (mouse/touch)
	{
		/*
		if (currentDeviceState == deviceStates.EditorState)
		{
			if (Input.GetMouseButtonDown(0))
			{
				currentInputState = inputStates.Began;
			}
			else if (Input.GetMouseButtonUp(0))
			{
				currentInputState = inputStates.Ended;
			}
		}
		*/
		//else //if (currentDeviceState == deviceStates.IphoneState)
		//{
			if (Input.touchCount > 0 )
			{
				Touch firstTouch = Input.touches[0];
				if (firstTouch.phase == TouchPhase.Began)
				{
					currentInputState = inputStates.Began;
				}
				else if (firstTouch.phase == TouchPhase.Moved)
				{
					currentInputState = inputStates.Moved;
				}
				else if (firstTouch.phase == TouchPhase.Stationary)
				{
					currentInputState = inputStates.Stationary;
				}
				else if (firstTouch.phase == TouchPhase.Ended)
				{
					currentInputState = inputStates.Ended;
				}
			} else 
			{
				if (Input.GetMouseButtonDown(0))
				{
					currentInputState = inputStates.Began;
				}
				else if (Input.GetMouseButtonUp(0))
				{
					currentInputState = inputStates.Ended;
				}	
			}
		//}
		
	}
	
	#endregion
	
	#region Update Function
	// Update is called once per frame
	void Update () 
	{
		AssignInputState();
		ResponceOnInputStateForSimpleGesture();
	}
	#endregion
}
