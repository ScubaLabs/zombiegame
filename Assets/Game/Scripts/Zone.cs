﻿using UnityEngine;
using System.Collections;

public class Zone : MonoBehaviour {

	private Transform thisTransform;
	private Transform cityBlock;

//	public GameObject tl;
//	public GameObject tm;
//	public GameObject tr;
//	public GameObject ml;
//	public GameObject mr;
//	public GameObject bl;
//	public GameObject bm;
//	public GameObject br;
//
//	private bool tlb;
//	private bool tmb;
//	private bool trb;
//	private bool mlb;
//	private bool mrb;
//	private bool blb;
//	private bool bmb;
//	private bool brb; 

	private float sizex;
	private float sizez;

	private bool activescene = false;
	// Use this for initialization
	void Awake()
	{
		thisTransform = GetComponent<Transform>(  );

	}

	void Start () {
		cityBlock = thisTransform.Find ("Block/Cube");
		sizex = cityBlock.GetComponent<Renderer>().bounds.size.x/4;
		sizez = cityBlock.GetComponent<Renderer>().bounds.size.z/4;
	}

	// Update is called once per frame
	void Update () {

		if(activescene == true) {

			// var posx = Player.instance.transform.position.x - thisTransform.position.x;
			// var posz = Player.instance.transform.position.z - thisTransform.position.z;
			
			// //Top Row
			// if(posz > sizez)
			// {
			// 	tmb = true;
			// } else {
			// 	tmb = false;
			// }
			
			// if(posx > (sizex*(-1)) && posz > sizez)
			// {
			// 	tlb = true;
			// } else {
			// 	tlb = false;
			// }
			
			// if(posx > sizex && posz > sizez)
			// {
			// 	trb = true;
			// } else {
			// 	trb = false;
			// }

			// if(tmb == true)
			// {
			// 	if(!GameObject.Find("tm"))
			// 	{

			// 		UpdateZones(tm,0,1);

			// 	}
			// } else {
			// 	if(GameObject.Find("tm"))
			// 	{
			// 		GameObject.Destroy(GameObject.Find("tm"));
			// 	}
			// }	
		}
			
			//Mid Row
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			activescene = true;
			GameManager.Instance.zone = thisTransform.gameObject;

		}
	}

	void OnTriggerExit(Collider other) {
		if(other.gameObject.tag == "Player")
		{
			activescene = false;
			GameManager.Instance.zone = null;

		}
	}
	
	void UpdateZones(GameObject obj, int x, int z)
	{
		Vector3 ppos = thisTransform.position;
		ppos.x += cityBlock.GetComponent<Renderer>().bounds.size.x * (x);
		ppos.z += cityBlock.GetComponent<Renderer>().bounds.size.z * (z);
		GameObject.Instantiate(obj, ppos, thisTransform.rotation);
		obj.name = "tm";
	}
}
