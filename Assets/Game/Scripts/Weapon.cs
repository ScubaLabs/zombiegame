using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
	
	public GameObject bulletPrefab;
	public AudioClip pistolShot;
	
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Fire(Vector3 target)
	{
		if(bulletPrefab != null)
		{
			Vector3 initpos = GameManager.Instance.player.transform.position;
			initpos.y -= 1.5f;
			AudioSource.PlayClipAtPoint(pistolShot, initpos);
			GameObject obj = Instantiate(bulletPrefab, initpos, Quaternion.identity) as GameObject;
			Bullet bullet = obj.GetComponent<Bullet>();
			
			bullet.targetDirection = (target - transform.position).normalized;
			bullet.range = 100;
			Debug.Log("Fire");
		}
	}
}
