﻿using UnityEngine;
using System.Collections;

public class BullDog : MonoBehaviour {

	public float xAxis = 0;
	public float yAxis = 0;
	private Transform thisTransform;
	private CharacterController character;
	public float jumpSpeed = 8;
	public float inAirMultiplier = 0.25f; 				// Limiter for ground speed while jumping
	public float protationSpeed = 300f;	// Camera rotation speed for each axis
	
	private Vector3 velocity; // Used for continuing momentum while in air
	private Vector3 prevForward;
	public Vector3 direction;
	public Vector3 moveCalc;

	private float smoothTime = 0.3f;
	private float zOffset = 1.0f;
	private float xOffset = 1.0f;

	public float moveSpeed = 30; //move speed
	public float rotationSpeed = 3; //speed of turning
	public float range = 10f;
	public float range2 = 10f;
	public float stop = 0;

	public float distance;




	// Use this for initialization
	void Start () {
		thisTransform = GetComponent<Transform>(  );
		character = GetComponent<CharacterController>(  );	
		direction = new Vector3(xAxis, 0, yAxis);
		character.Move(Player.instance.GetMovement(direction));
	}
	
	// Update is called once per frame
	void Update () {
		//thisTransform.position = Player.instance.transform.position;
		//rotate to look at the player
		var target = Player.instance.transform;
		direction = new Vector3(xAxis, 0, yAxis);
		var look = new Vector3 (target.position.x, thisTransform.position.y, target.position.z);
		thisTransform.LookAt(look); 
	    distance = Vector3.Distance(thisTransform.position, target.position);

		if (distance > 10) 
		{
			character.Move (thisTransform.forward * moveSpeed * Time.deltaTime);
		} else {
			GetComponent<Animation>().CrossFade("idle");
		}


	   
	


		//Player.instance.FaceMovementDirection();
	}
	///*
	void OnEnable(){
		EasyJoystick.On_JoystickMove += On_JoystickMove;
		EasyJoystick.On_JoystickMoveEnd += On_JoystickMoveEnd;
	}
	
	void OnDisable(){
		EasyJoystick.On_JoystickMove -= On_JoystickMove	;
		EasyJoystick.On_JoystickMoveEnd -= On_JoystickMoveEnd;
	}
	
	void OnDestroy(){
		EasyJoystick.On_JoystickMove -= On_JoystickMove;	
		EasyJoystick.On_JoystickMoveEnd -= On_JoystickMoveEnd;
	}
	///*
	void On_JoystickMoveEnd( MovingJoystick move){
		if (move.joystickName == "movePad"){
			xAxis = 0;
			yAxis = 0;
			//animation.CrossFade("idle");
		}
	}

	void On_JoystickMove( MovingJoystick move){
		
		if (move.joystickName == "movePad"){

			xAxis = move.joystickAxis.x;
			yAxis = move.joystickAxis.y;
			
			Vector3 movement = thisTransform.TransformDirection( new Vector3( xAxis, 0, yAxis ) );

				
			var yrun = false;
			var xrun = false;
			if ((Mathf.Abs(yAxis) < 0.5) && (Mathf.Abs(yAxis) > -0.5)){
				GetComponent<Animation>().CrossFade("walk");
				yrun = false;
			} else {
				GetComponent<Animation>().CrossFade("run");
				yrun = true;
			}
			
			if ((Mathf.Abs(xAxis) < 0.5) && (Mathf.Abs(xAxis) > -0.5)){
				GetComponent<Animation>().CrossFade("walk");
				xrun = false;
				
			} else {
				GetComponent<Animation>().CrossFade("run");
				xrun = true;
			}
			
			if(yrun == true || xrun == true)
			{
				GetComponent<Animation>().CrossFade("run");
			}

		}
	}
	//*/
}
