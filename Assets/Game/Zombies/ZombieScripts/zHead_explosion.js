
var radius = 3.0; // Radius of the explosion (this will be only invisible force, not an actual explosion)
var power = 200.0;
var bloodPrefab : GameObject;// This is a blood/gore particle.


/*
All of the code is in Awake because this script is attached to the
headless zombie prefab that will be spawned up a headshot death of a zombie.

*/
function Awake () {
	
	var explosionPos : Vector3 = transform.position;
	var colliders : Collider[] = Physics.OverlapSphere(explosionPos, radius); // Adds every collider in an created overlap sphere.
	
	
	// Blasts away several small bits of the sharded head.
	// Be wary of that this blast radius COULD affect other rigidbodies that are in the blast if the radius is too large.
	for (var hit : Collider in colliders){
		
		if (!hit){
			continue;
		}
		
		if (hit.GetComponent.<Rigidbody>() && hit.GetComponent.<Collider>().tag == "blastable"){
			hit.GetComponent.<Rigidbody>().AddExplosionForce(power, explosionPos, radius, 0); // The last float is how far below the explosion will be applied (0.2 meters below, resulting in upwards blast)
		}
		
	}
	
	// Spawn particle. Just to add some extra blood 'n gore to the blast!
	Instantiate(bloodPrefab, transform.position, transform.rotation);
	
}