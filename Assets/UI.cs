using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Threading;

public class UI : MonoBehaviour {
	
	//public static SQLiteDB db = null;
	private static string filename;
	private string log;

    string strUserName = "";
    string strgCurrentUser = "";

    public int selGridInt = 0;
    public int checkGridInt = 0;
    public string[] selStrings = new string[] { "Grid 1", "Grid 2", "Grid 3", "Grid 4" };
    public string[] arrUsers;
	public ArrayList arrlUsers = new ArrayList();
    public int intgCurrentLevel = 1;
    public string strgSourceLanguage = "en";
    public string strgTargetLanguage = "es";
    public string strgTranslation = "English - Spanish";
	private Player playerScript;
	private Transform playerTransform;


    // intgCurrentCard = Current Card ID
    public int intgCurrentCard = 0;
    public string strgResponse = "";
    public string strgAnswer = "";
    public string strgMessage = "";
	
	public GUIStyle customButton; 

    public DateTime dtmgTimer;

    public string strgRandom = "";

    public bool blngShowText;
    public bool blngShowSound;

    public int masteredQuestions = 0;
	public int questionsRemain = 0;

    public static UI instance;
	
	// variable to show mark sign
	public GameObject []AnswerSignMarks;
	
	// Use this for initialization
	void Start () {
		
		//db = new SQLiteDB();
				
		log = "";
		/*
		filename = GameManager.appPath + "/calltopower.db";
		
		if(!File.Exists(filename))
		{
			Debug.Log("DATABASE DOES NOT EXIST");
		}
		
		GameObject temp = GameObject.FindGameObjectWithTag("Player");
		if(temp != null)
		{
			playerTransform = temp.transform;
			playerScript = temp.GetComponent<Player>();
		}

        instance = this;

        // Recover from XML Dictionary and Queue Configs

        if (objgQueueConfigs == null)
        {
        }

        if (objgFlashCards == null)
        {
        }

        pageToDisplay = FlashCards.Structures.DisplayedPage.enLoginPage;
		
		
		SQLiteQuery qr;
		db.Open(filename);
	
		
		
		//
		// delete table if exists
		//
		qr = new SQLiteQuery(db, "SELECT * FROM gameusers;");
	
		arrUsers = new string[1];
        arrUsers[0] = "ListofUsers";

        
        int count = 0;
		while( qr.Step() )
		{
			string astring = qr.GetString("username");
			int userid = qr.GetInteger("id");
			arrlUsers.Add(astring);
			Debug.Log(astring);
			
			
			FlashCards.Structures.User objUser;
			objUser = new FlashCards.Structures.User();
            objUser.userName = astring;
			objUser.userid = userid;
            objUser.currentLevel = 1;
			
            objUser.password = "";
			
			objUsers.Add(objUser);
			
			count ++;
		}
									
		qr.Release();                                    
		
		//
		// if we reach that point it's mean we pass the test!
		db.Close();
				
			
        arrUsers = new string[objUsers.Count];

        arrlUsers.CopyTo(arrUsers);
        */
	}
	
	// Update is called once per frame
	void Update () {

        // if the selected User Profile is distinct to the current one
        if (selGridInt != checkGridInt)
        {

            strgCurrentUser = arrUsers[selGridInt];
			Debug.Log("UI::Update:Userint : " + selGridInt + " ,UserName : " + arrUsers[selGridInt]);

            checkGridInt = selGridInt;

            // Recover from XML the User Profile
					         

        } 
	}

    // Function to Play a clip file (mp3 or avi)
    public void PlayClip(string clipName)
    {
        AudioSource mysource;
        mysource = gameObject.AddComponent<AudioSource>();
        AudioClip myclip = UnityEngine.Resources.Load(clipName) as AudioClip;
        mysource.clip = myclip;
        mysource.Play();
    }

    // OnGUI is called once per frame
    void OnGUI()
    {
       
    }
	
	public void FadeOutResult(bool isCorrect)
    {
		GameObject objToSpawn;
		if (isCorrect)
		{
			Debug.Log( questionsRemain);
			
			objToSpawn = AnswerSignMarks[0];
		}
		else
		{
			// spawn cross mark GO
			objToSpawn = AnswerSignMarks[1];
		}
		GameObject.Instantiate(objToSpawn);
    }
	
		
	IEnumerator Download( WWW www )
	{
		yield return www;
	}
	
	
	public static void SelectQuery(string query)
	{
		//SQLiteQuery qr;
		
		//db.Open(filename); 
		
		////
		//// delete table if exists
		////
		//qr = new SQLiteQuery(db, query);
		//qr.Step();												
		//qr.Release();                                    
		
		////
		//// if we reach that point it's mean we pass the test!
		//db.Close();     

	}

}
